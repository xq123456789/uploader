const mongoose = require('mongoose')

const Image = mongoose.model('Image')
const Comment = mongoose.model('Comment')

module.exports = () => {
  const imagePromise = Image.aggregate([
    {
      $group: {
        _id: null,
        numOfImages: {$sum: 1},
        numOfLikes: {$sum: "$likes"},
        numOfVisit: {$sum: "$visit"}
      }
    }
  ])
  const commentPromise = Comment.find({}).count()
  return Promise.all([imagePromise, commentPromise])
    .then(([statsObj, commentCount]) => 
      ({...statsObj[0], numOfComments: commentCount})
    )
    // .then(
    //   ([
    //     {numOfImages, numOfLikes, numOfVisit}, commentCount
    //   ]) => ({
    //     numOfImages, numOfLikes, numOfVisit,
    //     numOfComments: commentCount
    //   })
    // )
    // .then(([statsObj, commentCount]) => {
    //   return {
    //     numOfImage: statsObj.numOfImages,
    //     numOfLikes: statsObj.numOfLikes,
    //     numOfVisit: statsObj.numOfVisit,
    //     numOfComments: commentCount
    //   }
    // })
}
