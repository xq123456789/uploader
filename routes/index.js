const router = require('koa-router')()
const mongoose = require('mongoose')
const statsfn = require('../lib/statsfn')
const Comment = mongoose.model('Comment')
const Image = mongoose.model('Image')

router.get('/', async (ctx, next) => {
  const images = await Image.find({})
    .sort({createdAt: -1}).limit(20)
  const stats = await statsfn()
  const recomments = await Comment.find()
  .sort({createdAt: -1})
  .limit(3)
  await ctx.render('index', {
    title: '图片共享', images, stats, recomments,user: ctx.session.user
  })
})

router.get('/login', async ctx => {
  await ctx.render('login')
})

router.post('/login', async ctx => {
  const {username, password} = ctx.request.body
  if(username === 'xqf' && password === '123') {
    ctx.session.user = {nickname: '许勤锋', username: 'xqf'}
    ctx.redirect('/')
  }
})

router.get('/logout', async ctx => {
  ctx.session.user = undefined
  ctx.redirect('/')
})
module.exports = router