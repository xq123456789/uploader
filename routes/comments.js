const router = require('koa-router')()
const mongoose = require('mongoose')

router.prefix('/comments')

const Image = mongoose.model('Image')
const Comment = mongoose.model('Comment')

router.post('/:imageid', async (ctx, next) => {
  const {email, nickname, comment} = ctx.request.body
  const image = await Image.findById(ctx.params.imageid)
  if(image) {
    await Comment.create({
      email, nickname, comment, image: image._id
    })
    ctx.redirect(`/images/show/${image._id}`)
  }
})

module.exports = router