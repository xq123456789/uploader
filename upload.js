const multer = require('koa-multer')

const upload = multer({dest: './public/uploads/'})

module.exports = upload