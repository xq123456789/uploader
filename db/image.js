const mongoose = require('mongoose')

const imageSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    maxlength: 128
  },
  description: {
    type: String,
    maxlength: 512
  },
  originalname: {
    type: String,
    maxlength: 128,
    required: true
  },
  mimetype: {
    type: String,
    required: true,
    enum: ['image/jpeg', 'image/jpg', 'image/png']
  },
  diskfilename: {
    type: String,
    required: true
  },
  likes: {
    type: Number,
    required: true,
    default: 0
  },
  visit: {
    type: Number,
    required: true,
    default: 0
  }
}, {timestamps: true})

mongoose.model('Image', imageSchema, 'image')