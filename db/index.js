const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/uploader')

mongoose.connection.on('error', err => console.log(err))
mongoose.connection.once('open', () => console.log('mongoose connected.'))

require('./image')
require('./comment')