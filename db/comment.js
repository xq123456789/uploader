const mongoose = require('mongoose')
const Schema = mongoose.Schema

const commentSchema = new Schema({
  email: {
    type: String, required: true,
    match: /^[a-zA-Z0-9_]{3,20}@[a-zA-Z0-9_.]+/
  },
  nickname: {
    type: String, required: true,
    maxlength: 64
  },
  comment: {
    type: String, required: true,
    trim: true, maxlength: 512
  },
  image: {
    type: Schema.Types.ObjectId,
    required: true
  }
}, {timestamps: true})

mongoose.model('Comment', commentSchema, 'comment')